package bytestream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.SequenceInputStream;

public class SequenceInputStreamDemo {

    public static void main(String[] args) throws IOException {

        FileOutputStream fileOutputStream1=new FileOutputStream("/tmp/1.txt");
        fileOutputStream1.write("anurag kr".getBytes());

        FileOutputStream fileOutputStream2=new FileOutputStream("/tmp/2.txt");
        fileOutputStream2.write(" anurag kr".getBytes());

        fileOutputStream1.flush();
        fileOutputStream2.flush();

        FileInputStream inputStream1=new FileInputStream("/tmp/1.txt");
        FileInputStream inputStream2=new FileInputStream("/tmp/2.txt");
        FileInputStream inputStream3=new FileInputStream("/tmp/2.txt");

        SequenceInputStream inputStream=new SequenceInputStream(inputStream1,inputStream2) ;

        int i=0;
        while ((i=inputStream.read())!=-1){
            System.out.print((char)i);
        }
/*
        Vector<FileInputStream> list=new Vector<>();
        list.add(inputStream1);
        list.add(inputStream2);
        list.add(inputStream3);

        SequenceInputStream sequenceInputStream=new SequenceInputStream(list.elements()) ;*/



System.out.println();
    }
}
