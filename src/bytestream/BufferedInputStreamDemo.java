package bytestream;

import java.io.*;

public class BufferedInputStreamDemo {

    public static void main(String[] args) throws IOException {

        BufferedOutputStream bufferedOutputStream=new BufferedOutputStream(
                new FileOutputStream("/tmp/abc.txt")
        );

        bufferedOutputStream.write("sanjeev".getBytes());
        bufferedOutputStream.flush();
        bufferedOutputStream.close();

        BufferedInputStream bufferedInputStream=new BufferedInputStream(
                new FileInputStream("/tmp/abc.csv"));

        int i=0;
        while ((i= bufferedInputStream.read())!=-1){
            System.out.println((char)i);
        }
    }
}
