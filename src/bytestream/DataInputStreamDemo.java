package bytestream;

import java.io.*;

public class DataInputStreamDemo {
    public static void main(String[] args) throws IOException {

        DataOutputStream dataOutputStream=new DataOutputStream(new FileOutputStream("/tmp/abc/user4.txt"));
        dataOutputStream.writeInt(12);
//        dataOutputStream.write("13".getBytes());
//        dataOutputStream.write("14".getBytes());
        dataOutputStream.flush();
        dataOutputStream.close();

        DataInputStream dataInputStream=new DataInputStream(new FileInputStream("/tmp/abc/user4.txt"));

        int i=0;

        System.out.println(dataInputStream.readInt());
//        while ((i=dataInputStream.readInt())!=-1){
////            dataInputStream.readLine();
////            dataInputStream.readChar();
////            System.out.println((char)i);
//            System.out.println();
//      }
    }
}
