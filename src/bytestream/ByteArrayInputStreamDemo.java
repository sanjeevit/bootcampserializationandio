package bytestream;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteArrayInputStreamDemo {

    public static void main(String[] args) throws IOException {

       ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
       String name="sanjeev kumar jha";
       byteArrayOutputStream.write(name.getBytes());

       byteArrayOutputStream.writeTo(new FileOutputStream("/tmp/abc.text"));
       byteArrayOutputStream.writeTo(new FileOutputStream("/tmp/cde.text"));

        byteArrayOutputStream.flush();
        byteArrayOutputStream.close();
    }
}
