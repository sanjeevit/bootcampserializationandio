package bytestream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInputStreamDemo {

    public static void main(String[] args) throws IOException {
//        FileOutputStream fileOutputStream=new FileOutputStream("/tmp/file.txt");
        FileOutputStream fileOutputStream=new FileOutputStream(new File("/tmp/file.txt"));

        String name="manoj kumar";
        byte byteArray[]=name.getBytes();

        for(byte b:byteArray){
            fileOutputStream.write(b);
        }
        fileOutputStream.close();

//        fileOutputStream.write(byteArray);

        int i=0;
        FileInputStream fileInputStream=new FileInputStream("/tmp/file.txt");
        while ((i=fileInputStream.read())!=-1){
            System.out.print((char)i);
        }
        fileInputStream.close();
    }
}
