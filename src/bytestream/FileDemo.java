package bytestream;

import java.io.File;
import java.io.IOException;

public class FileDemo {

    public static void main(String[] args) throws IOException {

        File file=new File("/tmp/abc/pqr/");

//        create single directory at a timr
//        file.mkdir();

//        create multiple directory
        file.mkdirs();

        File file1=new File("/tmp/abc/pqr/abc.txt");
        if (!file1.exists()){
            file1.createNewFile();
        }
    }
}
