package characterstream;

import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;

public class CharArrayWriterDemo {
    public static void main(String[] args) throws IOException {
        CharArrayWriter charArrayWriter=new CharArrayWriter();
        charArrayWriter.write("sanjeev");

//        charArrayWriter.writeTo(new FileWriter(""));
        CharArrayReader charArrayReader=new CharArrayReader(charArrayWriter.toCharArray());

        int i=0;

        while ((i=charArrayReader.read())!=-1){
            System.out.println((char)i);
        }

    }
}
