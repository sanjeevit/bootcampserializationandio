package characterstream;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterDemo {

    public static void main(String[] args) throws IOException {

        FileWriter fileWriter=new FileWriter("/tmp/xyz.txt");
        fileWriter.write("sanjeev".toCharArray());
        fileWriter.write("kumar");
        fileWriter.write("jha");

        fileWriter.flush();
        fileWriter.close();
        FileReader fileReader=new FileReader("/tmp/xyz.txt");

        int i=0;
        while ((i=fileReader.read())!=-1){
            System.out.println((char)i);
        }
    }
}
