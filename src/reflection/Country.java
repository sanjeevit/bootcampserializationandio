package reflection;

public final class Country {
    public String name;
    public String code;
    private String group;

   public Country(){
        System.out.println("default constructor");
    }

    public Country(String name){
        System.out.println("one parameter constuctor");
        this.name=name;
    }

    private Country(String name,String code){
        System.out.println("two parameter constructor");
        this.name=name;
        this.code=code;
    }
}
