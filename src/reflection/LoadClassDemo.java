package reflection;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class LoadClassDemo {

    public static void main(String[] args) throws Exception {

//        load a class in the memory
        Class<Country> aClass= (Class<Country>) Class.forName("reflection.Country");

        System.out.println(aClass.getName());

        System.out.println(aClass.newInstance());

        Class superClass=aClass.getSuperclass();
        System.out.println(superClass);

        Class interfaceClass[]=aClass.getInterfaces();
        System.out.println(interfaceClass);

        int modifier=aClass.getModifiers();
        if(Modifier.isPublic(modifier)){
            System.out.println("public");
        }

        Field fields[]=aClass.getFields();
        System.out.println();
        for(Field f:fields){
            System.out.println("tyep:"+f.getType().getName());
            System.out.println("name:"+f.getName());
        }




//        Constructor constructor=Class.forName("reflection.Country").getDeclaredConstructor();
//        constructor.setAccessible(true);
//        Country country=(Country) constructor.newInstance();
//        System.out.println(country);

//       Constructor constructor[]= Class.forName("reflection.Country").getConstructors();
//
//       constructor[0].setAccessible(true);
//        Country country=(Country)Class.forName("reflection.Country").newInstance();





    }
}
