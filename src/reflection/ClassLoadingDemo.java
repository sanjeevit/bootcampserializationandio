package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ClassLoadingDemo  {

    public static void main(String args[]) throws Exception{
        new ClassLoadingDemo().invokeConstructor();
    }

    public void load() throws ClassNotFoundException{

        Class aClass=Class.forName("reflection.Country");

        Class<Country> aClass1=(Class<Country>) Class.forName("reflection.Country");
    }

    public void printClassName() throws ClassNotFoundException{
        Class<Country> aClass1=(Class<Country>) Class.forName("reflection.Country");
        System.out.println(aClass1.getName());
    }

    public void createNewInstance() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class aClass=Class.forName("reflection.Country");
        Country country=(Country)aClass.newInstance();
    }

    public void getSuperClass() throws Exception{
        Class aClass=Class.forName("reflection.Country");
        Class superClass=aClass.getSuperclass();
    }

    public void getInterface() throws Exception{
        Class aClass=Class.forName("reflection.Country");
        Class superClass[]=aClass.getInterfaces();
    }

    public void printModifier() throws Exception{
        Class aClass=Class.forName("reflection.Country");
        int modifier=aClass.getModifiers();

        System.out.println("is public:"+Modifier.isPublic(modifier));
        System.out.println("is final:"+Modifier.isFinal(modifier));
        System.out.println("is private:"+Modifier.isPrivate(modifier));
    }

    public void printFields() throws Exception{
        Class aClass=Class.forName("reflection.Country");

//        aClass.getFields(); returns public fields
        for(Field field:aClass.getFields()){
            System.out.println("Type="+field.getType());
            System.out.println("name="+field.getName());
        }

//       aClass.getDeclaredFields(); returns all the fields
        for(Field field:aClass.getDeclaredFields()){
            System.out.println("Type="+field.getType());
            System.out.println("name="+field.getName());
        }
    }

    public void changeAccesibilityCons() throws Exception {
        Class<Country> aClass = (Class<Country>) Class.forName("reflection.Country");
        aClass.getConstructor().setAccessible(true);
    }

    public void invokeConstructor() throws Exception{
        Class<Country> aClass = (Class<Country>) Class.forName("reflection.Country");
        aClass.getConstructor(new Class[]{String.class}).newInstance("sanjeev");
    }

    public void getConstructors() throws Exception{
        Class<Country> aClass = (Class<Country>) Class.forName("reflection.Country");

        for(Constructor constructor:aClass.getDeclaredConstructors()){
            constructor.getParameters()[0].getName();
            constructor.getParameters()[0].getType();
        }
    }

}
