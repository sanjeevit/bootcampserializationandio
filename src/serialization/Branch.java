package serialization;

public class Branch extends Office {

    private String address;

    public Branch(){
        System.out.println("default constructor called of Branch");
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
