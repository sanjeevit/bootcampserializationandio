package serialization;

import java.io.Serializable;

public class Office /*implements Serializable*/ {

    private String dept;

    public Office(){
        System.out.println("default constructor called of Office");
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Office{" +
                "dept='" + dept + '\'' +
                '}';
    }
}
