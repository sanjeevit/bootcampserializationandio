package serialization;

import java.io.Serializable;

public class Employee1 extends Branch implements Serializable {

    private String name;
    private int age;
    private Employee2 employee2;

    public Employee1(){
        System.out.println("default constructor called of employee1");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Employee1{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
