package serialization;

import java.io.Serializable;

public class Employee2 implements Serializable {
//    private static final long serialVersionUID = 1L;

    private String name;
    private int age;
    private Office office;

    public Employee2(){
        System.out.println("default constructor called of Employee2");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    @Override
    public String toString() {
        return "Employee2{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", office=" + office +
                '}';
    }
}
