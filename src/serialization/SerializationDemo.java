package serialization;

import java.io.*;

public class SerializationDemo {
    public static void main(String[] args) throws Exception {
        serlization();
        externalization();
    }

    public long getSerialVersionUid(Employee1 employee1){
        return ObjectStreamClass.lookup(employee1.getClass()).getSerialVersionUID();
    }

    public static void serlization() throws Exception{
        Employee1 employee1=new Employee1();
        employee1.setName("sanjee");
        employee1.setAge(29);

        ObjectOutputStream objectOutputStream=
                new ObjectOutputStream(new FileOutputStream("/tmp/abc/user6.txt"));
        objectOutputStream.writeObject(employee1);
        objectOutputStream.flush();
        objectOutputStream.close();

        ObjectInputStream stream = new ObjectInputStream(
                new FileInputStream("/tmp/abc/user6.txt"));
        Employee1 employee2=(Employee1) stream.readObject();
        System.out.println(employee1);
        stream.close();
    }
    public static void externalization() throws Exception{

        Employee3 employee3=new Employee3();
        employee3.setName("sanjeev");
        employee3.setDept("IT");

        ObjectOutputStream objectOutputStream=new ObjectOutputStream(
                new FileOutputStream("/tmp/abc/user8.txt"));

        objectOutputStream.writeObject(employee3);
        objectOutputStream.flush();
        objectOutputStream.close();

        Employee3  employee=(Employee3 )new ObjectInputStream(
                new FileInputStream("/tmp/abc/user8.txt"))
                .readObject();
        System.out.println(employee);
    }
}
