package serialization;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerializationDemo {
    public static void main(String[] args) {


        try(ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream("/tmp/employee1.txt"))){
            Employee1 employee1=(Employee1) objectInputStream.readObject();
            System.out.println(employee1);
            Employee1 employee2=(Employee1) objectInputStream.readObject();
            System.out.println(employee2);

        }catch (Exception exc){
             exc.printStackTrace(System.out);
        }

        /*try(ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream("/tmp/employee2.txt"))){
            Employee2 employee=(Employee2) objectInputStream.readObject();
            System.out.println(employee);
        }catch (Exception exc){
             exc.printStackTrace(System.out);
        }*/
    }
}
